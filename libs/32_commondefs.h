#ifndef _32_COMMONDEFS_H_
#define _32_COMMONDEFS_H_

typedef enum {
    E_OK,
    E_PARAM_PROBLEM,
    E_NOMATCH,
    E_EXISTS,
    E_ERR= -1
} err_t; 

#define MAX_SIZE  128
#define KEYSPACE  "db"
#define MASTER    "master"
#define USERS     "users"
#define EMPLOYERS "employers"
#define STOP      "stop"
#define JSONBRACE "{\n"
#define JSONSTART "\t\""
#define JSONCOLON "\":"
#define JSONCOMMA ",\n"
#define JSONSTOP  "}"
#define STOPSIZE  4 
#define SALT_SIZE 32 // Refer to pratical crytography for size discussion
#define UUID_SIZE 16
#define MAX_VIDEO_SIZE 1024
#define MAX_NAME 1024



#endif 
