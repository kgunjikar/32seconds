#include <QtCassandra/QCassandra.h>
#include <QtCore/QDebug>
#include <thrift-gencpp-cassandra/cassandra_types.h>
#include <openssl/sha.h>
#include "32_cassandra.h"
#include <qjson/serializer.h>
#include <qjson/parser.h>

void *
memory_alloc (int size)
{
    void *tmp;   

    tmp = malloc(size);
    if (tmp == NULL) { 
        assert(0);
    }
    memset(tmp, 0, size);
    return(tmp);
}

void 
mkjson_resume (QString& resume,
               QUuid id, QtCassandra::QCassandraValue v)
{
    QByteArray json;
    QVariantMap temp;
    QVariantList list;
    QJson::Serializer serial;
    QJson::Parser parser;
    bool ok;
    
    temp = parser.parse(v.binaryValue(), &ok).toMap();
    if (!ok) { 
        qFatal("Error decoding JSON");
        assert(0);
    } 
    temp.insert("id", id.toString());
    list << temp;
    
    json = serial.serialize(list);
    qDebug() << json;
    resume = QString(json);
}

void 
mkjson_video (QString& video,
              QUuid id,
              QtCassandra::QCassandraValue v
             )
{
    QByteArray json;
    QVariantMap temp;
    QVariantList list;
    QJson::Serializer serial;
    QJson::Parser parser;
    bool ok;

    temp = parser.parse(v.binaryValue(), &ok).toMap();
    if (!ok) { 
        qFatal("Error decoding JSON");
        assert(0);
    }
    temp.insert("id", id.toString());
    
    list << temp;
    
    json = serial.serialize(list);
    qDebug() << json;
    video = QString(json);
}

void 
mkjson_user (QString& user,
             QUuid id,
             QtCassandra::QCassandraValue f,
             QtCassandra::QCassandraValue l,
             QtCassandra::QCassandraValue paid,
             QtCassandra::QCassandraValue pc
            )
{
    QByteArray json;
    QVariantMap temp;
    QVariantList list;
    QJson::Serializer serial;
    
    temp.insert("id", id.toString());
    temp.insert("firstname", f.stringValue());
    temp.insert("lastname", l.stringValue());
    temp.insert("paid", paid.binaryValue());
    temp.insert("pc", pc.binaryValue());
    
    list << temp;
    
    json = serial.serialize(list);
    user = QString(json);
}

#ifdef TEST
    int 
main(int argc, char *argv[])
{
    char userdata[512];

    char id[]= "88ed9c29-58c6-4ec0-bcd3-a18f41b91fc6";

    libcass_init ("localhost", "SimpleStrategy", 1);

    if (libcass_login("test@rest.com","Ehh", userdata) == E_OK) {
        qDebug()<<"login  success";
    } else {
        qDebug()<<"Unable to login";
    }
    if (libcass_signup("test@rest.com","Ehh", "test", "rest",FALSE) == E_OK) { 
        qDebug()<<"signup success";
    } else {
        qDebug()<<"Unable to signup";
    }
    
    libcass_getuserinfo(id, userdata);  
    libcass_getvideoinfo(id, userdata);  
    libcass_getresumeinfo(id, userdata);  

    qDebug() << "This is the set test ";

    libcass_setresinfo("Kshitij.txt",id);  
    libcass_setresinfo("Kshitij.txt",id);  
    libcass_setvideoinfo("32vid.mp4", id);  
    libcass_setvideoinfo("32vid.mp4", id);  
 
    libcass_close();   
}
#endif 

