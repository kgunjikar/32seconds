#ifndef _32_CASSANDRA_H_
#define _32_CASSANDRA_H_ 

#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include "32_commondefs.h"

/* Function Definitions */
void libcass_dump_data ();

int libcass_getresumeinfo(char *uuid, char *resumedata);

int libcass_getvideoinfo(char *uuid, char *videodata);

int libcass_setresinfo(char* resume_add, char *uuid);

int libcass_setvideoinfo(char* video_add, char *uuid);

int libcass_getuserinfo(char *uuid, char *userdata);

int libcass_close();

int libcass_init(const char *host, const char *strategy, 
                 int repl_factor);

int libcass_table_create(const char *tablename, const char *col_type,
                         const char *validation_class, 
                         const char *comparator_type, 
                   QSharedPointer <QtCassandra::QCassandraTable>& table,
                         char ptr [][MAX_SIZE]);

int libcass_login(const char *email, const char *password,
                  char *userdata);

int libcass_signup(const char *email, const char * passwd,
                   const char *first, const char *last,
                   bool is_employer);

extern void *memory_alloc(int size);

/* Extern Functions */
extern "C" { 
    extern int gen_password(const char *string, char *digest, 
                            char *salt);
}
extern bool validate_email(const char *s);

#endif /* _32_CASSANDRA_H_ */
