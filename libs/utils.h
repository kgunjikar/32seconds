#ifndef _UTILS_H_
#define _UTILS_H_

void mkjson_resume(QString& resume, QUuid id, 
                   QtCassandra::QCassandraValue v);

void mkjson_video(QString& video, QUuid id,
                  QtCassandra::QCassandraValue v);

void mkjson_user(QString& user, QUuid id,
                 QtCassandra::QCassandraValue f,
                 QtCassandra::QCassandraValue l,
                 QtCassandra::QCassandraValue paid,
                 QtCassandra::QCassandraValue pc);

#endif /* _UTILS_H_ */
