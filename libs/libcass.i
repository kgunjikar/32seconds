%module libcass

%{
int libcass_getresumeinfo(char *uuid, char *resumedata);

int libcass_getvideoinfo(char *uuid, char *videodata);

int libcass_setresinfo(char* resume_add, char *uuid);

int libcass_setvideoinfo(char* video_add, char *uuid);

int libcass_getuserinfo(char *uuid, char *userdata);

int libcass_close();

int libcass_init(const char *host, const char *strategy, 
                 int repl_factor);

int libcass_login(const char *email, const char *password,
                  char *userdata);

int libcass_signup(const char *email, const char * passwd,
                   const char *first, const char *last,
                   bool is_employer);

%}

int libcass_getresumeinfo(char *uuid, char *resumedata);

int libcass_getvideoinfo(char *uuid, char *videodata);

int libcass_setresinfo(char* resume_add, char *uuid);

int libcass_setvideoinfo(char* video_add, char *uuid);

int libcass_getuserinfo(char *uuid, char *userdata);

int libcass_close();

int libcass_init(const char *host, const char *strategy, 
                 int repl_factor);

int libcass_login(const char *email, const char *password,
                  char *userdata);

int libcass_signup(const char *email, const char * passwd,
                   const char *first, const char *last,
                   bool is_employer);
