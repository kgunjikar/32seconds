#include <stdio.h>
#include <string.h>
#include <openssl/sha.h>
#include <openssl/rand.h>
#include <assert.h>
#include <malloc.h>
#include "32_commondefs.h"


    int 
gen_password (const char *string, 
        char *digest, char *salt)
{
    char *temp_str = NULL; 
    unsigned int size; 

#ifdef DEBUG
    memset(digest, 0x41, SHA512_DIGEST_LENGTH);
    memset(salt, 0x41, SALT_SIZE);

#else
    size = strlen(string) + SALT_SIZE;    

    temp_str = (char *) malloc(size);
    if (temp_str == NULL) {
        assert(0);
    }

    RAND_bytes((unsigned char *) salt, SALT_SIZE);

    memcpy(temp_str, string, strlen(string));
    memcpy(temp_str + strlen(string), salt, SALT_SIZE);

    SHA512((unsigned char*)temp_str, size, (unsigned char*)digest);    

    free(temp_str);

#endif 
    return (E_OK);
}
