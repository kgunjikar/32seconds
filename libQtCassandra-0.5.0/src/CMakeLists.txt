#
# Text:
#      CMakeLists.txt
#
# Description:
#      Defines the source files.
#
# Documentation:
#      See the CMake documentation.
#
# License:
#      Copyright (c) 2011-2013 Made to Order Software Corp.
#
#      http://snapwebsites.org/
#      contact@m2osw.com
#
#      Permission is hereby granted, free of charge, to any person obtaining a
#      copy of this software and associated documentation files (the
#      "Software"), to deal in the Software without restriction, including
#      without limitation the rights to use, copy, modify, merge, publish,
#      distribute, sublicense, and/or sell copies of the Software, and to
#      permit persons to whom the Software is furnished to do so, subject to
#      the following conditions:
#
#      The above copyright notice and this permission notice shall be included
#      in all copies or substantial portions of the Software.
#
#      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

project( QtCassandra )

get_property( THRIFT_HEADER_FILES GLOBAL PROPERTY thrift-gencpp-cassandra_HEADER_FILES )
get_property( THRIFT_SOURCE_FILES GLOBAL PROPERTY thrift-gencpp-cassandra_SOURCE_FILES )

add_library(${PROJECT_NAME} SHARED
    QCassandra.cpp
    QCassandraPrivate.cpp
    QCassandraContext.cpp
    QCassandraTable.cpp
    QCassandraColumnDefinition.cpp
    QCassandraColumnPredicate.cpp
    QCassandraLock.cpp
    QCassandraRow.cpp
    QCassandraRowPredicate.cpp
    QCassandraCell.cpp
    QCassandraValue.cpp
    QCassandraConsistencyLevel.cpp

    # Low level thrift code
    #
    ${THRIFT_HEADER_FILES}
    ${THRIFT_SOURCE_FILES}
)


target_link_libraries(QtCassandra
    thrift
    ${QT_LIBRARIES}
)

install(
    TARGETS QtCassandra LIBRARY DESTINATION lib
)
install(
    DIRECTORY ${libQtCassandra_SOURCE_DIR}/include/QtCassandra
    DESTINATION include
    FILES_MATCHING PATTERN *.h
    PATTERN .svn EXCLUDE
)
install(
    FILES ${libQtCassandra_BINARY_DIR}/include/QtCassandra/QCassandra.h
    DESTINATION include/QtCassandra/
)

# vim: ts=4 sw=4 et
