Cassandra Library

The intention of the cassandra library is to isolate the backend and the front end. 
The code is written in C++ using libQTCassandra-0.5.0.

Doxygen Link: http://libqtcassandra.sourceforge.net/

The cassandra library makes thrift calls to the cassandra server. 

Required to compile code
controlledvariables: http://sourceforge.net/projects/controlled-vars/

APache Thrift and the QT4.8 onwards. Ubuntu 12.04 was used in testing



FrontEnd
--------------

Its in PHP for now. Testing


Install
------------
To use the cassandra library install thrift and libQtcassandra. 

Installing Thrift (Ubuntu 12.04)
-----------------------------------------
sudo apt-get install libboost-dev libboost-test-dev libboost-program-options-dev libevent-dev 
automake libtool flex bison pkg-config g++ libssl-dev 

0. cd <blah>/thrift

1. ./bootstrap.sh

2. ./configure --enable-libtool-lock 

3. make (if there are problems because of parallel make , use make -j 1).

4. make check

5. sh test/test.sh
 
6. make install (need root permissions)
 
 Installing libQtCassandra 
 ------------------------------------
 0. sudo apt-get install cmake 
 
 1. mkdir <whereever>/BUILD
 2. cd BUILD
 3. cmake ../libQtCassandra
 4. make 
 5. sudo make install 
 
 Installing cassandra
 ----------------------------
0. sudo vim /etc/apt/sources.list

#Add at the bottom
deb http://www.apache.org/dist/cassandra/debian 20x main
deb-src http://www.apache.org/dist/cassandra/debian 20x main

Run an apt-get update. 

sudo apt-get update

This will give you a warning about not being able to verify the signatures of the apache repos:

GPG error: http://www.apache.org unstable Release:
The following signatures couldn't be verified because the public key is not available:
NO_PUBKEY 4BD736A82B5C1B00

Now do the following for that key:

gpg --keyserver pgp.mit.edu --recv-keys 4BD736A82B5C1B00
gpg --export --armor 4BD736A82B5C1B00 | sudo apt-key add -

Also add this one:

gpg --keyserver pgp.mit.edu --recv-keys 2B5C1B00
gpg --export --armor 2B5C1B00 | sudo apt-key add -

Now run apt-get update again.
sudo apt-get update

The error should be gone. Now check that all is working and UBuntu can see Cassandra 2.0:

apt-cache showpkg cassandra
Package: cassandra
Versions:
2.0.1

Great! Now install it:

sudo apt-get install cassandra

Reference: http://christopher-batey.blogspot.com/2013/10/installing-cassandra-20-on-ubuntu.html
