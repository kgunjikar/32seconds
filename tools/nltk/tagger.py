#!/usr/bin/python -tt

'''
# This code uses POS taggers to extracts POS from a given text. The POS
# tagging will generate a F-SCORE. The F-SCOREs for a given set of corpora
# will be plotted and a k-means clustering done. 
# This will be used for calculating the classification of new data based on
# the training examples.
# 
# Using NLTK to start with POS Taggers. Using the UPENN set.
#
# Refrences:
# 1. Formality of Language: definition, measurement and behavioral determinants
# FRANCIS HEYLIGHEN* & JEAN-MARC DEWAELE** 1999
#
# 2. 
'''

import nltk ;


def tagger(n, op):
  text = nltk.word_tokenize(n);
  a = nltk.pos_tag(text);
  op.write(str(a));

if __name__=='__main__':
  fp = open ('input.txt', 'r');
  op = open ('output.txt', 'w');

  for line in fp:
      tagger(line, op);

  fp.close();
  op.close();
