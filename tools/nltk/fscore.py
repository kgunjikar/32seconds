#!/usr/bin/python -tt

'''
# This code uses POS taggers to extracts POS from a given text. The POS
# tagging will generate a F-SCORE. The F-SCOREs for a given set of corpora
# will be plotted and a k-means clustering done. 
# This will be used for calculating the classification of new data based on
# the training examples.
# 
# Using NLTK to start with POS Taggers. Using the UPENN set.
#
# Refrences:
# 1. Formality of Language: definition, measurement and behavioral determinants
# FRANCIS HEYLIGHEN* & JEAN-MARC DEWAELE** 1999
#
'''

import nltk ;

if __name__=='__main__':
  op = open ('output.txt', 'r');

  counts = nltk.defaultdict(int);

  for line in op:
    for word in line.split(")"):
      try :
        position = word.index('(');
      except:
        continue;
      tup =  word[position + 1:];
      string = tup.split(',');
      word = string[0];
      tag = string[1];
      x = tag.replace(" ", "");
      counts[x] += 1;
      print x;

  for key in counts.keys():
    print "key: %s , value: %d" % (key, counts[key]);
  
  adj_score = counts[str('JJ')] + counts[str('JJR')] + counts[str('JJS')]; 
  print adj_score;
  noun_score = counts['NN'] + counts['NNS'] + counts['NNP'] + counts['NNPS']; 
  verb_score = counts['VB'] + counts['VBD'] + counts['VBG'] + counts['VBN'] + counts['VBP'] + counts['VBZ']; 
  adverb_score = counts['RB'] + counts['RBR'] + counts['RBS'] + counts['WRB'];
  pronoun_score = counts['PRP'] + counts['PRP$'] + counts['WP'] + counts['WP$'];

  op.close();
