#!/usr/bin/python -tt
import os
import re
from collections import defaultdict
import nltk 
import csv

OUTPUT = 'output.txt'
PATH = '/home/kshitijgunjikar/txtresumes'
eduset = ['Bachelor', 'master','PhD','Doctor','MS', 'BS', 'MSc', 'BSc', 'BE','B.E','ME','M.E','B.Sc','M.Sc'];
innoset = ['paper', 'Patent','PTO','github','bitbucket'];
techset = ['C','C++','Java','Python']
MRC_FEATURE_DICT = "./mrc.txt"
MRC_NOS_FEATURES = 13
SECOND_PASS = "./secondpass.txt"



def find_edu(line):
  for i in eduset:
    if re.match(i,line, re.I) :
      outfile.writerow(str(i))

def find_inno(line):
  if line == 'Paper':
    print line
  for i in innoset:
    if re.search(i,line, re.I) :
      outfile.writerow(str(i))

def tagger(n, FSCORE):
  text = nltk.word_tokenize(n);
  a = nltk.pos_tag(text);
  for string in a:
    word = string[0];
    tag = string[1];
    if tag == 'JJ' or tag == 'JJR' or tag == 'JJS' :
      FSCORE[0] += 1
    if tag == 'NN' or tag == 'NNS' or  tag == 'NNP' or tag ==  'NNPS':
      FSCORE[1] += 1
    if  tag == 'VB' or tag == 'VBD' or tag == 'VBG' or tag == 'VBN' or tag == 'VBP' or tag == 'VBZ': 
      FSCORE[2]+= 1
    if tag == 'RB' or  tag == 'RBR' or tag == 'RBS' or  tag == 'WRB':
      FSCORE[3]+= 1
    if tag == 'PRP' or tag == 'PRP$' or tag == 'WP' or tag == 'WP$':
      FSCORE[4] += 1

 
def find_fscore(line, FSCORE) :
  tagger(line, FSCORE) 

def find_mrc(line, COUNTS):
  words = line.split();
  i = 0;
  while i < len(words) :
    if words[0].upper() in mrc_dict:
      x = words[0].upper();
      j = 0
      y = mrc_dict[x]
      while j < MRC_NOS_FEATURES -1 :
        COUNTS[j] += int(y[j])
        j = j + 1 
    i = i + 1

def find_ttr(f) :
  ttr_list = []
  word_count = 0
  sentence_count = 0
  for line in f:
    a = nltk.word_tokenize(line)
    for string in a:
      word_count = word_count + 1 
      if string == "." :
        sentence_count = sentence_count + 1
      if string in ttr_list:
        continue
      else:
        ttr_list.append(string)
  ttr = len(ttr_list) / float(word_count);
  wps = word_count / float(sentence_count)
  print ttr, wps, word_count
  # Type to token ratio
  #outfile.writerow(ttr)
  # Word count 
  #outfile.writerow(token) 
  # WPS
  #outfile.writerow(wps)
  
    

def search_in_file(path):
  try:
    f = open(path,'r')
    linenos = 0;
    COUNTS = [0 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 , 0]
    FSCORE = [0, 0, 0, 0, 0]
    find_ttr(f)
    for line in f:
      linenos = linenos + 1
      find_edu(line);
      find_inno(line);
      find_mrc(line, COUNTS);
      find_fscore(line, FSCORE);
    outfile.writerow( COUNTS );
    #outfile.writerow( FSCORE );
    f.close()
  except IOError, e:
     print 'cannot open', path
     print os.strerror(e.errno)

def add_to_dict(temp):
  i = 1 
  while i < MRC_NOS_FEATURES:
      mrc_dict[temp[0]].append(temp[i]);
      i = i + 1;

'''
  XXX: FIXME 
  Its possible the parsing will not happen properly
  Hence make sure we have 13 in a row only. Treat 
  the rest in the second pass.
  Also make sure the purge duplicates
'''
def load_mrc(mrc_dict):

  mrcfp = open(MRC_FEATURE_DICT,'r');
  second = open(SECOND_PASS,"w");
  
  for line in mrcfp:
    temp = line.split() # Change this line
    if len(temp) > MRC_NOS_FEATURES + 1: 
      dummy = temp[0] + temp[1]
      finaldictval = [0] * (13)
      finaldictval[1:] = temp[2:]
      finaldictval[0] = dummy 
      if len(finaldictval) > MRC_NOS_FEATURES:
        continue
      add_to_dict(finaldictval)

    if temp[0] in mrc_dict:
      continue;
    add_to_dict(temp);
  #for val in mrc_dict.items():
  #  print val
  mrcfp.close()
  second.close()

def main():
  global mrc_dict;
  mrc_dict = defaultdict(list);

  load_mrc(mrc_dict);

  filelist = os.listdir(PATH)
  global outfile ;
  wr = open(OUTPUT,'w+');
  outfile = csv.writer(wr, dialect='excel')
  
  for i in filelist:
    x = PATH + '/' + i
    currentfile = os.path.basename(x)
    wr.write(currentfile[0:-4] + " ")
    search_in_file(x)
    wr.write('\n')

  wr.close()
if __name__ == '__main__':
  main()

