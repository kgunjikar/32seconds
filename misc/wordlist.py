#!/usr/bin/python -tt

from nltk.corpus import wordnet as wn
import nltk


def generate_similar(fp):
  text = nltk.Text(word.lower() for word in nltk.corpus.brown.words())
  for line in fp:
    print line
    print text.similar(line.lower())

def main(seed, fp):
  count = 0
  for i in seed:
    for ss in wn.synsets(i):
      word = (ss.name).split(".")[0]
      if word  not in seed:
        seed.append(word)
        print word
        count = count + 1
  fp.write(str(seed))

if __name__=='__main__':
  goal_seed=['ambition','intention','objective','achieved', 'aim','aspirations','success','performance','feat','attainment', 'target']
  fp = open('goal.txt','w+')
  fp.write(str(goal_seed))
  main(goal_seed, fp)
  fp.close()
