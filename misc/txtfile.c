#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAX     1024
#define MAXEDU  14
#define MAXINNO 5
#define FALSE   (1==0)
#define TRUE    (1==1)

char buf[MAX];

typedef struct inno_s
{
    char string[MAX];
    float weight;
} inno_t;

const char *edustr[MAXEDU] = {"Bachelor", "BS","BSc","BE","B.E","B.Sc","master","MS","MSc","ME","M.E","M.Sc","PhD","Doctor"};

inno_t innostr[MAXINNO] = {
    {.string = "paper",.weight = 0.8},
    {.string = "PTO",.weight = 1.2},
    {.string = "patent",.weight = 1.2},
    {.string = "github",.weight = 0.4},
    {.string = "bitbucket",.weight = 0.4},
};

static double y  = 0;
static double x  = 0;

void 
process_str(const char *string)
{
    bool setpatent = FALSE;
    bool setgithub = FALSE;
    int  i;
    inno_t temp;
    double r;

    if (string) {
//	printf("%s\n", string);
    } else {
        return ;
    }
    
    printf("%f",r);
    for (i = 0; i < MAXINNO; i++) {
	if (strcmp(innostr[i].string, string) == 0) {
            printf("\n %s", innostr[i].string);
            temp = innostr[i];
	    x = x + temp.weight + r ;	
	}
    }
    r = (double) rand() / RAND_MAX;
    for (i = 0; i < MAXEDU; i++) {
	if (strcmp(edustr[i], string) == 0) {
          printf("\n %s", edustr[i]);
	  if ( i > ceil(y)) {
               y = (double)i + r ;
          } 
	}
    }
    printf(" x = %f y = %f \n",x, (float) y +r);
}

main()
{
    FILE *output, *input;
    char *string;

    srand(time(NULL));

    input = fopen("output.txt", "r");
    if (input == NULL) {
	printf("\n Cant open file");
	goto end;
    }
    output = fopen("xy.dat", "w");
    if (output == NULL) {
	printf("\n Cant open file");
	goto end;
    }

    while (!feof(input)) {
	memset(buf, 0, sizeof(buf));
	fscanf(input, "%s", buf);
	string = strtok(buf, ",");
        y = (double) rand() / RAND_MAX;
        x = (double) rand() / RAND_MAX;
	process_str(string);
	while (string = strtok(NULL, ",")) {
	    process_str(string);
	}
        fprintf(output,"%f %f\n", x, (float) y);
    }

  end:
    printf("\n Exting");
    fclose(input);
    fclose(output);
}
