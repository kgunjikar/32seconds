#!/usr/bin/python -tt

import re
import sys
import os

matched_files = [];
option = 1; # start from 1

def execute_cmd(cmd_string):
  os.system("clear")
  a = os.system(cmd_string)
  print ""
  if a == 0:
    print "Command executed correctly"
  else:
    print "Command terminated with error"
  print ""

#The cscope.files gives us the list of files  
#where we are to search for a regex
#
def main(argv):
  try:
    f = open(argv[0],'r')
    # clear list before every search
    del matched_files[0:len(matched_files)]
    for line in f:
      search_in_file(line,argv)
    if len(matched_files) > 0:
      inp = raw_input('Enter option:')
    else:
      print 'Regex didn\'t match '
    inp = int(inp)
    if inp >0 & inp < len(matched_files): 
      string =""
      string += "vim "
      string += str(matched_files[inp])
      execute_cmd(string)
  except IOError, e:
     print 'cannot open', argv[0], 'adios'
     print os.strerror(e.errno)
  except:
     f.close()


#For each file print the full path and line number
# where we found the regex. Give an option to open 
# it in VIM(or whatever).

def search_in_file(files,argv):
  try:
    filename = files.rstrip()
    f = open(filename,'r')
    linenos = 0;
    for line in f:
      linenos = linenos + 1
      if re.findall(argv[1], line): 
        print line
        print os.getcwd()+str(filename[filename.find('/'):])+':'+str(linenos)+':'
        print line
        # Truncate the filename of starting char and get entire
        # path of file to open
        string = os.getcwd()+ filename[filename.find('/'):]
        string += ' +'
        string += str(linenos)
        matched_files.append(str(string))
        option = option + 1;
  except IOError, e:
     print 'cannot open', files
     print os.strerror(e.errno)
  except:
     f.close()

if __name__=='__main__':
  if len(sys.argv) < 2 :
    print'Usage: search.py <cscope.files> <search string>'

  main(sys.argv[1:])
