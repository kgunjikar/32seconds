import pyPdf  
import os

PATH = '/home/kshitij/resumes'

def read_pdf(path):
    content = ""
    num_pages = 10
    p = file(path, "rb")
    pdf = pyPdf.PdfFileReader(p)
    for i in range(9, num_pages):
        x = pdf.getPage(i).extractText()+'\n' 
        content += x

    content = " ".join(content.replace(u"\xa0", " ").strip().split())     
    return content

filelist = os.listdir(PATH)
print filelist

for i in filelist:
  x = PATH + '/' + i
  print x
  con = read_pdf(x)
  print con
