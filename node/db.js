/*
 * Users.js -- DB code
 *
 * Dec, 2013, Kshitij Gunjikar
 * Copyright 2013-2018, 32seconds.com
 */

var Db = require('mongodb').Db;
var Connection = require('mongodb').Connection;
var Server = require('mongodb').Server;
var BSON = require('mongodb').pure.BSON;
var ObjectID = require('mongodb').ObjectID;
var assert = require('assert');

Users = function(host, port) {
  console.log("\n in the init function")
  this.db= new Db('top32', new Server(host, port, {safe: false}, {auto_reconnect: true}, {}));
  this.db.open(function(){});
};


Users.prototype.get_collection= function(callback) {
  console.log("\n in the get  collection function")
  this.db.collection('Users', function(error, employee) {
    if( error ) callback(error);
    else callback(null, employee);
  });
};

//find all employees
Users.prototype.find_all = function(callback) {
  console.log("\n in the findall function")
  this.get_collection(function(error, employee) {
    if( error ) callback(error)
      else {
        employee.find().toArray(function(error, results) {
          if( error ) callback(error)
          else callback(null, results)
        });
      }
    });
};

//save new employee
Users.prototype.save = function(employees, callback) {
  console.log("\n in the save function")
  this.get_collection(function(error, employee) {
    if( error ) callback(error)
    else {
      if( typeof(employees.length)=="undefined")
        employees = [employees];

      for( var i =0;i< employees.length;i++ ) {
        employee = employees[i];
        employee.created_at = new Date();
      }

      employee_collection.insert(employees, function() {
        callback(null, employees);
      });
    }
  });
};

/*find an employee by ID*/
Users.prototype.find_by_id = function(id, callback) {
  this.get_collection(function(error, Users_collection) {
    if( error ) callback(error)
    else {
      Users_collection.findOne({_id: Users_collection.db.bson_serializer.ObjectID.createFromHexString(id)}, function(error, result) {
        if( error ) callback(error)
        else callback(null, result)
      });
    }
  });
};

/*
// update an employee
EmployeeProvider.prototype.update = function(employeeId, employees, callback) {
  this.getCollection(function(error, employee_collection) {
    if( error ) callback(error);
    else {
      employee_collection.update(
	{_id: employee_collection.db.bson_serializer.ObjectID.createFromHexString(employeeId)},
           employees,
	  function(error, employees) {
	    if(error) callback(error);
	    else callback(null, employees)       
	  });
      }
  });
};

//delete employee
EmployeeProvider.prototype.delete = function(employeeId, callback) {
  this.getCollection(function(error, employee_collection) {
    if(error) callback(error);
      else {
	employee_collection.remove(
			{_id: employee_collection.db.bson_serializer.ObjectID.createFromHexString(employeeId)},
function(error, employee){
 if(error) callback(error);
else callback(null, employee)
});
}
});
};

//find an employee by ID
EmployeeProvider.prototype.findById = function(id, callback) {
  this.getCollection(function(error, employee_collection) {
    if( error ) callback(error)
    else {
      employee_collection.findOne({_id: employee_collection.db.bson_serializer.ObjectID.createFromHexString(id)}, function(error, result) {
        if( error ) callback(error)
        else callback(null, result)
      });
    }
  });
};

*/

exports.Users = Users;
