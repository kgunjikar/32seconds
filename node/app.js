/*
 * app.js -- Server code
 *
 * Dec, 2013, Kshitij Gunjikar
 * Copyright 2013-2018, 32seconds.com
 */
var express = require('express');
var path = require('path');
var util = require('util');
var mongoose = require("mongoose");
var videoserver = require("vid-streamer");
var videosettings = {
             rootPath: '.',
             forceDownload: false
             } ;

mongoose.connect("mongodb://localhost/top32");

var user = new mongoose.Schema({
  firstname: String,
  lastname : String,
  email    : String,
  password : String,
  
});

var userdata = mongoose.model('User',user);
 
var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 8080);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.set('view options', {layout: false});
  app.set(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(path.join(__dirname, 'public')));
  videoserver.settings(videosettings);
});

app.get('/',function(req, res) {
    console.log("\n Default get");
    res.render('index');
}); 

/* Login */
app.post('/login',function(req, res) {
  console.log("\n User login");
  return userdata.find(function(err, user) {
    if (!err) { 
      res.render('user', user);
      console.log(user);
    } else {
      console.log("Error finding user");
    }
  });
}); 

/* Register new user */
app.post('/signup',function(req, res) {
  console.log("\n Register new user : "+ util.inspect(req.body.email));
  console.log("\n Headers: "+ util.inspect(req.headers));
  var user = new userdata({
    firstname:req.body.fname,
    lastname:req.body.lname,
    email:req.body.email,
    password:req.body.pwd,
   });
  return user.save(function(err) {
    if (!err) {
      return console.log('Created \n');
    } else {
      return console.log('Error \n');
    }
  }); 
}); 

app.listen(app.get('port'));
